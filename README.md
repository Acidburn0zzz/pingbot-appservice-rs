# pingbot-appservice-rs

Matrix pingbot appservice in Rust

## Install

- Compile: `cargo build`.
- Generate two access tokens (`HS_TOKEN` and `AS_TOKEN`). e.g. using `pwgen`.
- Set up as a system service. Using systemd: modify `systemd/pingbot.example.service` and `systemd/pingbot.example.socket` and install them e.g. in `/etc/systemd/system/`.
- Modify the appservice file `appservice.example.yaml`, adding the access tokens, and register it with your homeserver.
- Optionally register with the homeserver, using `AS_TOKEN`. This returns another access token which you can use optionally instead of `$AS_TOKEN`:
```
curl -d '{"username":"pingbot", "type": "m.login.application_service"}' "https://mobilizon.us/_matrix/client/v3/register?access_token=$AS_TOKEN"
```
## Adding to rooms

Joining rooms must be done manually. Here is a command to do it via [Client-Server API)](https://spec.matrix.org/v1.6/client-server-api/#post_matrixclientv3joinroomidoralias):
```
curl -d '{}' "https://mobilizon.us/_matrix/client/v3/rooms/%21ROOM_ID:example.org/join?access_token=$AS_TOKEN"
```

The appservice responds in rooms to the `!ping` command.

## Development

The appservice runs a HTTP server, using systemd socket activation to receive the server socket. When not run via systemd, you can emulate this by using [systemfd](https://github.com/mitsuhiko/systemfd); further, you can use [cargo-watch](https://github.com/watchexec/cargo-watch) for auto-reloading during development, as follows:
```
cargo install systemfd cargo-watch
systemfd --no-pid -s http::7001 -- cargo watch -qx run
```
If run without systemd/systemfd, the appservice listens on a random port.
A benefit of using the socket activation is that requests received during recompilation do not fail but are served when the compilation succeeds and the program starts.

## References

- https://spec.matrix.org/v1.2/application-service-api/
- https://github.com/maubot/echo/blob/master/echo.py
- https://github.com/maubot/pingstat
- https://gitlab.com/famedly/conduit/-/blob/next/APPSERVICES.md
- https://pad.mau.fi/code/#/2/code/view/bEQSTiIid1O-GkVqpMED9g70E8dZKH+M4wsxp1ntvbY/embed/present/ (Echo/Ping Bot Spec)

## Repo hosts
- [ssb://%31GZTczroR1ldeWb0BbZH10MCWh9dd5HQM9qiN734YY=.sha256](https://git.scuttlebot.io/%2531GZTczroR1ldeWb0BbZH10MCWh9dd5HQM9qiN734YY%3D.sha256)
- https://codeberg.org/cel2/pingbot-appservice-rs

## License

AGPL3+
