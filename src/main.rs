/*
 * pingbot-appservice-rs
 * Copyright (C) 2023 Charles E. lehner
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use anyhow::{anyhow, Context, Error};
use chrono::{Duration, Utc};
use hyper::header::{AUTHORIZATION, CONTENT_TYPE};
use hyper::http::{Request, Response};
use hyper::{server::conn::Http, service::service_fn, Body, Method, StatusCode};
use serde_json::{json, Map, Value};
use std::collections::HashMap;
use std::env::var;
use std::net::SocketAddr;
use tokio::net::TcpListener;
use tokio::net::TcpStream;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let mut listenfd = listenfd::ListenFd::from_env();
    let listener = match listenfd.take_tcp_listener(0)? {
        Some(listener) => TcpListener::from_std(listener)?,
        None => {
            let bind_addr = SocketAddr::from(([127, 0, 0, 1], 0));
            TcpListener::bind(bind_addr).await?
        }
    };
    let base_url = format!("http://{}", listener.local_addr()?);
    println!("Listening on {}", base_url);

    loop {
        let (tcp_stream, _client_addr) = listener.accept().await?;
        tokio::task::spawn(async move {
            if let Err(http_err) = Http::new()
                .http1_only(true)
                .http1_keep_alive(true)
                .serve_connection(tcp_stream, service_fn(server_main))
                .await
            {
                eprintln!("Error while serving HTTP connection: {}", http_err);
            }
        });
    }
}

async fn server_main(req: Request<Body>) -> Result<Response<Body>, Error> {
    // omit query from log because it may contain access token
    let path = req.uri().path().to_string();
    let hs_token = var("HS_TOKEN").context("unable to get HS_TOKEN")?;
    let query = req.uri().query().unwrap_or_default();
    let params = url::form_urlencoded::parse(query.as_bytes())
        .into_owned()
        .collect::<HashMap<String, String>>();
    eprintln!("{} {}", req.method(), path);
    if params.get("access_token").map(String::as_str) != Some(&hs_token)
        && match req.headers().get(AUTHORIZATION) {
            Some(auth_header_value) => auth_header_value
                .to_str()
                .context("Unable to parse Authorization header")?
                .strip_prefix("Bearer "),
            None => None,
        } != Some(&hs_token)
    {
        eprintln!("unauthorized");
        return Response::builder()
            .status(StatusCode::UNAUTHORIZED)
            .body(Body::from("Unauthorized (unmatched access token)\n"))
            .context("serving unauthorized");
    }
    if let Some(txid) = path.strip_prefix("/_matrix/app/v1/transactions/") {
        serve_tx(req, txid).await
    } else {
        serve_not_found()
    }
}

async fn serve_tx(req: Request<Body>, _txid: &str) -> Result<Response<Body>, Error> {
    if req.method() != Method::PUT {
        return Response::builder()
            .status(StatusCode::METHOD_NOT_ALLOWED)
            .header(CONTENT_TYPE, "application/json")
            .body(Body::from(r#"{"error": "Expected PUT"}"#))
            .context("serving not found");
    }
    let data = hyper::body::to_bytes(req).await.context("read PUT data")?;
    let mut obj: Map<String, Value> = serde_json::from_slice(&data).context("parsing PUT data")?;
    if let Some(Value::Array(events)) = obj.remove("events") {
        for event in events {
            if let Some(e) = handle_event(event).await.err() {
                eprintln!("error handling event: {}", e);
            }
        }
    }
    Response::builder()
        .body(Body::from("OK"))
        .context("handling PUT")
}

async fn handle_event(event: Value) -> Result<(), Error> {
    let event_type = event
        .get("type")
        .and_then(|value| value.as_str())
        .ok_or(anyhow!("missing event type"))?;
    if event_type == "m.room.message" {
        let content = event
            .get("content")
            .and_then(|value| value.as_object())
            .ok_or(anyhow!("missing content"))?;
        let body = content
            .get("body")
            .and_then(|value| value.as_str())
            .ok_or(anyhow!("missing body"))?;
        let msgtype = content
            .get("msgtype")
            .and_then(|value| value.as_str())
            .ok_or(anyhow!("missing msgtype"))?;
        if msgtype != "m.text" {
            return Ok(());
        }
        if let Some(arg) = body.strip_prefix("!ping") {
            let arg = arg.trim().to_string();
            if let Err(e) = handle_ping(event, &arg).await {
                eprintln!("error responding to ping: {}", e);
            }
        } else if let Some(arg) = body.strip_prefix("!echo") {
            let arg = arg.trim().to_string();
            if let Err(e) = handle_echo(event, &arg).await {
                eprintln!("error responding to echo: {}", e);
            }
        }
    } else {
        eprintln!(
            "unknown event: {}",
            serde_json::to_string_pretty(&event).context("serialize")?
        );
    }
    Ok(())
}

fn format_duration(mut duration: Duration) -> String {
    let ms = duration.num_milliseconds();
    if ms < 10000 {
        return format!("{} ms", ms);
    }
    if ms < 60000 {
        return format!("{:.2} seconds", (ms as f64) / 1000.0);
    }
    let mut parts = Vec::new();
    let days = duration.num_days();
    if days == 1 {
        parts.push(String::from("1 day"));
    } else if days > 1 {
        parts.push(format!("{} days", days));
    }
    duration = duration - Duration::days(days);

    let hours = duration.num_hours();
    if hours == 1 {
        parts.push(String::from("1 hour"));
    } else if hours > 1 {
        parts.push(format!("{} hours", hours));
    }
    duration = duration - Duration::hours(hours);

    let minutes = duration.num_minutes();
    if minutes == 1 {
        parts.push(String::from("1 minute"));
    } else if minutes > 1 {
        parts.push(format!("{} minutes", minutes));
    }
    duration = duration - Duration::minutes(minutes);

    let seconds = duration.num_seconds();
    if seconds == 1 {
        parts.push(String::from("1 second"));
    } else if seconds > 1 {
        parts.push(format!("{} seconds", seconds));
    }

    if let Some(last) = parts.pop() {
        if parts.is_empty() {
            return last.to_string();
        }
        return parts.join(", ") + " and " + &last;
    } else {
        format!("{}", duration)
    }
}

async fn handle_ping(event: Value, arg: &str) -> Result<(), Error> {
    let event_id = event
        .get("event_id")
        .and_then(|value| value.as_str())
        .ok_or(anyhow!("missing event id"))?;
    let room_id = event
        .get("room_id")
        .and_then(|value| value.as_str())
        .ok_or(anyhow!("missing room id"))?;
    let origin_server_ts = event
        .get("origin_server_ts")
        .and_then(|value| value.as_i64())
        .ok_or(anyhow!("missing origin_server_ts"))?;
    let origin_server_time = chrono::NaiveDateTime::from_timestamp_millis(origin_server_ts)
        .ok_or(anyhow!("unable to convert origin_server_ts"))?
        .and_local_timezone(Utc)
        .single()
        .ok_or(anyhow!("unable to convert server time to single"))?;

    // respond with pong
    let sender = event
        .get("sender")
        .and_then(|value| value.as_str())
        .ok_or(anyhow!("missing sender"))?;
    let from = sender.rsplit(':').next().unwrap();
    let now = Utc::now();
    let duration = now - origin_server_time;
    let duration_string = format_duration(duration);
    let duration_ms = duration.num_milliseconds();
    let (quoted_arg, html_quoted_arg) = if arg.is_empty() {
        (String::new(), String::new())
    } else {
        (
            format!(" \"{}\"", arg),
            format!(" \"{}\"", html_escape::encode_text(arg)),
        )
    };
    let body = format!(
        "{}: Pong! (ping{} took {} to arrive",
        sender, quoted_arg, duration
    );
    let formatted_body = format!(
        r#"<a href="https://matrix.to/#/{}">{}</a>: Pong! (<a href="https://matrix.to/#/{}/{}">ping</a>{} took {} to arrive)"#,
        sender,
        html_escape::encode_text(sender),
        room_id,
        event_id,
        html_quoted_arg,
        duration_string
    );
    let out = json!({
      "msgtype": "m.notice",
      "body": body,
      "format": "org.matrix.custom.html",
      "formatted_body": formatted_body,
      "m.relates_to": {
        "rel_type": "xyz.maubot.pong",
        "event_id": event_id,
        "from": from,
        "ms": duration_ms,
      },
      "pong": {
        "ping": event_id,
        "from": from,
        "ms": duration_ms,
      }
    });
    send(room_id, out).await
}

fn generate_txid() -> String {
    format!("m{}", Utc::now().timestamp_millis())
}

async fn handle_echo(event: Value, arg: &str) -> Result<(), Error> {
    let room_id = event
        .get("room_id")
        .and_then(|value| value.as_str())
        .ok_or(anyhow!("missing room id"))?;
    let out = json!({
      "msgtype": "m.notice",
      "body": arg,
    });
    send(room_id, out).await
}

async fn send(room_id: &str, out: Value) -> Result<(), Error> {
    let as_token = var("AS_TOKEN").context("unable to get AS_TOKEN")?;
    let uri = format!(
        "/_matrix/client/r0/rooms/{}/send/m.room.message/{}",
        room_id,
        generate_txid()
    );
    let json = serde_json::to_string_pretty(&out).context("serialize event")?;
    let mx_http_server = var("MX_HTTP_SERVER").context("unable to get MX_HTTP_SERVER")?;
    let stream = TcpStream::connect(mx_http_server).await?;
    let (mut sender, conn) = hyper::client::conn::handshake(stream)
        .await
        .context("http handshake")?;
    tokio::task::spawn(async move {
        if let Err(err) = conn.await {
            println!("Connection failed: {:?}", err);
        }
    });
    let req = Request::builder()
        .uri(uri)
        .method(Method::PUT)
        .header(CONTENT_TYPE, "application/json")
        .header(AUTHORIZATION, format!("Bearer {}", as_token))
        .body(Body::from(json))?;
    let res = sender.send_request(req).await?;
    if res.status() != StatusCode::OK {
        println!("Response status: {}", res.status());
    }
    let data = hyper::body::to_bytes(res)
        .await
        .context("read POST response")?;
    let obj: Map<String, Value> = serde_json::from_slice(&data).context("parsing response")?;
    let json = serde_json::to_string_pretty(&obj).context("serialize response")?;
    eprintln!("response: {}", json);
    Ok(())
}

fn serve_not_found() -> Result<Response<Body>, Error> {
    Response::builder()
        .status(StatusCode::NOT_FOUND)
        .body(Body::from("Not found\n"))
        .context("serving not found")
}
